<?php

$realm = 'Prom 2013 Song Suggestions Admin';

$users = array(
	'ryan.tse' => 'rt-prstm-security01',
	'bryan.tran' => 'bt-pbltm-security02',
	'lauren-cooper' => 'lc-plscm-security03',
	'michelle-tam' => 'mt-pmstm-security04',
	'vahini-patel' => 'vp-pvspm-security05',
	'rachel-bian' => 'rb-prsbm-security06'
);

if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
	header('HTTP/1.1 401 Unauthorized');
	header('WWW-Authenticate: Digest realm="'.$realm.
			'",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');

	die('Unauthorized login. An administrator has been notified of this event.');
}

if (!($data = http_digest_parse($_SERVER['PHP_AUTH_DIGEST'])) ||
	!isset($users[$data['username']])) {
	header('HTTP/1.1 401 Unauthorized');
	header('WWW-Authenticate: Digest realm="'.$realm.
			'",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');
	die('Unauthorized login. An administrator has been notified of this event.');
}

$A1 = md5($data['username'] . ':' . $realm . ':' . $users[$data['username']]);
$A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
$valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);

if ($data['response'] != $valid_response) {
	header('HTTP/1.1 401 Unauthorized');
	header('WWW-Authenticate: Digest realm="'.$realm.
			'",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');
	die('Unauthorized login. An administrator has been notified of this event.');
}

function http_digest_parse($txt)
{
	$needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
	$data = array();
	$keys = implode('|', array_keys($needed_parts));

	preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

	foreach ($matches as $m) {
		$data[$m[1]] = $m[3] ? $m[3] : $m[4];
		unset($needed_parts[$m[1]]);
	}

	return $needed_parts ? false : $data;
}

if($_REQUEST["action"]=="query") {
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: POST");
	header("Access-Control-Allow-Headers: *");
	header("Content-Type: application/json");

	require("./mysql_catalyst.php");
	require("./mysql_alchemy.php");
	require("./mysql_blackmagic.php");

	$database = MySQLDatabase::getInstance();
	$database->connect("localhost","ihsc2014_voting","FMRxFbJDLqWjwdvZ", "ihs_c2014songvoting");

	$return_array = array();

	switch($_REQUEST["view"]) {
		case "statistics":
			$query = "SELECT COUNT(*) FROM song_list WHERE NOT EXISTS (SELECT * FROM ban_artist WHERE ban_artist.artist_id = song_list.artist_id) AND NOT EXISTS (SELECT * FROM ban_track WHERE ban_track.track_id = song_list.track_id) AND NOT EXISTS (SELECT * FROM ban_ip WHERE ban_ip.ip = song_list.ip_address)";
			$return_array["acceptedSubmissions"] = $database->fetchOne($query);
			$query = "SELECT COUNT(*) FROM song_list";
			$return_array["totalSubmissions"] = $database->fetchOne($query);;
			break;

		case "censored-suggestions":
			$query = "SELECT song_list.track_id, search_track.track_censored as track_name_censored, search_track.track_uncensored as track_name_uncensored, song_list.artist_id, search_artist.artist_censored AS artist_name_censored, search_artist.artist_uncensored AS artist_name_uncensored, COUNT(*) as count FROM song_list INNER JOIN track_list AS search_track ON song_list.track_id = search_track.track_id INNER JOIN artist_list AS search_artist ON song_list.artist_id = search_artist.artist_id WHERE NOT EXISTS (SELECT * FROM ban_artist WHERE ban_artist.artist_id = song_list.artist_id) AND NOT EXISTS (SELECT * FROM ban_track WHERE ban_track.track_id = song_list.track_id) AND NOT EXISTS (SELECT * FROM ban_ip WHERE ban_ip.ip = song_list.ip_address) GROUP BY track_id ORDER BY count DESC";
			foreach ($database->iterate($query) as $row) {
				$return_array["results"][] = array(
					"song_id" => $row->track_id,
					"song_name" => $row->track_name_censored,
					"artist_id" => $row->artist_id,
					"artist_name" => $row->artist_name_censored,
					"count" => $row->count
				);
			}
			$return_array["resultCount"] = count($return_array["results"]);
			break;

		case "uncensored-suggestions":
			$query = "SELECT song_list.track_id, search_track.track_censored as track_name_censored, search_track.track_uncensored as track_name_uncensored, song_list.artist_id, search_artist.artist_censored AS artist_name_censored, search_artist.artist_uncensored AS artist_name_uncensored, COUNT(*) as count FROM song_list INNER JOIN track_list AS search_track ON song_list.track_id = search_track.track_id INNER JOIN artist_list AS search_artist ON song_list.artist_id = search_artist.artist_id WHERE NOT EXISTS (SELECT * FROM ban_artist WHERE ban_artist.artist_id = song_list.artist_id) AND NOT EXISTS (SELECT * FROM ban_track WHERE ban_track.track_id = song_list.track_id) AND NOT EXISTS (SELECT * FROM ban_ip WHERE ban_ip.ip = song_list.ip_address) GROUP BY track_id ORDER BY count DESC";
			foreach ($database->iterate($query) as $row) {
				$return_array["results"][] = array(
					"song_id" => $row->track_id,
					"song_name" => $row->track_name_uncensored,
					"artist_id" => $row->artist_id,
					"artist_name" => $row->artist_name_uncensored,
					"count" => $row->count
				);
			}
			$return_array["resultCount"] = count($return_array["results"]);
			break;

		case "suggestions":
			$query = "SELECT song_list.id, song_list.track_id, search_track.track_censored as track_name, song_list.artist_id, search_artist.artist_censored AS artist_name, song_list.ip_address FROM song_list INNER JOIN track_list AS search_track ON song_list.track_id = search_track.track_id INNER JOIN artist_list AS search_artist ON song_list.artist_id = search_artist.artist_id WHERE NOT EXISTS (SELECT * FROM ban_artist WHERE ban_artist.artist_id = song_list.artist_id) AND NOT EXISTS (SELECT * FROM ban_track WHERE ban_track.track_id = song_list.track_id) AND NOT EXISTS (SELECT * FROM ban_ip WHERE ban_ip.ip = song_list.ip_address) ORDER BY id DESC";
			foreach ($database->iterate($query) as $row) {
				$return_array["results"][] = array(
					"song_id" => $row->track_id,
					"song_name" => $row->track_name,
					"artist_id" => $row->artist_id,
					"artist_name" => $row->artist_name,
					"ip_address" => $row->ip_address
				);
			}
			$return_array["resultCount"] = count($return_array["results"]);
			break;

		case "artists":
			$query = "SELECT * FROM artist_list WHERE NOT EXISTS (SELECT * FROM ban_artist WHERE ban_artist.artist_id = artist_list.artist_id) ORDER BY artist_uncensored";
			foreach($database->iterate($query) as $row) {
				$return_array["results"][] = array(
					"artist_id" => $row->artist_id,
					"uncensored" => $row->artist_uncensored,
					"censored" => $row->artist_censored
				);
			}
			$return_array["resultCount"] = count($return_array["results"]);
			break;

		case "banned-artists":
			$query = "SELECT ban_artist.artist_id AS artist_id, search_artist.artist_censored as artist_name FROM ban_artist INNER JOIN artist_list AS search_artist ON ban_artist.artist_id = search_artist.artist_id";
			foreach ($database->iterate($query) as $row) {
				$return_array["results"][] = array(
					"artist_id" => $row->artist_id,
					"artist_name" => $row->artist_name
				);
			}
			$return_array["resultCount"] = count($return_array["results"]);
			break;

		case "banned-songs":
			$query = "SELECT ban_track.track_id AS song_id, search_track.track_censored as song_name FROM ban_track  INNER JOIN track_list AS search_track ON ban_track.track_id = search_track.track_id";
			foreach ($database->iterate($query) as $row) {
				$return_array["results"][] = array(
					"song_id" => $row->song_id,
					"song_name" => $row->song_name
				);
			}
			$return_array["resultCount"] = count($return_array["results"]);
			break;

		case "banned-ips":
			$query = "SELECT * FROM ban_ip";
			foreach ($database->iterate($query) as $row) {
				$return_array["results"][] = array(
					"ip" => $row->ip
				);
			}
			$return_array["resultCount"] = count($return_array["results"]);
			break;

		case "modify":
			switch($_REQUEST["type"]) {
				case "artist":
					$query = "UPDATE artist_list SET artist_censored='".mysql_real_escape_string($_REQUEST["value"])."' WHERE artist_id='".mysql_real_escape_string($_REQUEST["target"])."'";
					$database->query($query);
					break;
			}
			break;

		case "remove":
			switch($_REQUEST["type"]) {
				case "artist":
					$query = "INSERT INTO ban_artist (artist_id) VALUES ('".mysql_real_escape_string($_REQUEST["target"])."')";
					$database->query($query);
					break;

				case "song":
					$query = "INSERT INTO ban_track (track_id) VALUES ('".mysql_real_escape_string($_REQUEST["target"])."')";
					$database->query($query);
					break;

				case "ip":
					$query = "INSERT INTO ban_ip (ip) VALUES ('".mysql_real_escape_string($_REQUEST["target"])."')";
					$database->query($query);
					break;
			}
			break;

		case "unban":
			switch($_REQUEST["type"]) {
				case "artist":
					$query = "DELETE FROM ban_artist WHERE artist_id='".mysql_real_escape_string($_REQUEST["target"])."'";
					$database->query($query);
					break;

				case "song":
					$query = "DELETE FROM ban_track WHERE track_id='".mysql_real_escape_string($_REQUEST["target"])."'";
					$database->query($query);
					break;

				case "ip":
					$query = "DELETE FROM ban_ip WHERE ip='".mysql_real_escape_string($_REQUEST["target"])."'";
					$database->query($query);
					break;
			}
			break;
	}
	die(json_encode($return_array));
} else if ($_REQUEST["action"] == "generate") {
	set_time_limit(90);

	require("./mysql_catalyst.php");
	require("./mysql_alchemy.php");
	require("./mysql_blackmagic.php");
	require("./tcpdf/tcpdf.php");

	$database = MySQLDatabase::getInstance();
	$database->connect("localhost","ihsc2014_voting","FMRxFbJDLqWjwdvZ", "ihs_c2014songvoting");

	$pdf_controller = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf_controller->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf_controller->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	$pdf_controller->SetFontSubsetting(false);
	$pdf_controller->setPrintHeader(false);
	$pdf_controller->setPrintFooter(false);
	$pdf_controller->SetFont("DejaVuSerif", "", 10);
	$pdf_controller->AddPage();

	$html  = '<h2>Song List</h2><table cellspacing="0" cellpadding="1" border="1">';
	$html .= "<tr><td><b>Song</b></td><td><b>Artist</b></td></tr>";
	switch($_REQUEST["view"]) {
		case "censored":
			$query = "SELECT song_list.track_id, search_track.track_censored as track_name_censored, search_track.track_uncensored as track_name_uncensored, song_list.artist_id, search_artist.artist_censored AS artist_name_censored, search_artist.artist_uncensored AS artist_name_uncensored, COUNT(*) as count FROM song_list INNER JOIN track_list AS search_track ON song_list.track_id = search_track.track_id INNER JOIN artist_list AS search_artist ON song_list.artist_id = search_artist.artist_id WHERE NOT EXISTS (SELECT * FROM ban_artist WHERE ban_artist.artist_id = song_list.artist_id) AND NOT EXISTS (SELECT * FROM ban_track WHERE ban_track.track_id = song_list.track_id) AND NOT EXISTS (SELECT * FROM ban_ip WHERE ban_ip.ip = song_list.ip_address) GROUP BY track_id ORDER BY count DESC";
			foreach ($database->iterate($query) as $row) {
				$html .= "<tr><td>".utf8_encode($row->track_name_censored)."</td><td>".utf8_encode($row->artist_name_censored)."</td></tr>";
			}
			break;

		case "uncensored":
			$query = "SELECT song_list.track_id, search_track.track_censored as track_name_censored, search_track.track_uncensored as track_name_uncensored, song_list.artist_id, search_artist.artist_censored AS artist_name_censored, search_artist.artist_uncensored AS artist_name_uncensored, COUNT(*) as count FROM song_list INNER JOIN track_list AS search_track ON song_list.track_id = search_track.track_id INNER JOIN artist_list AS search_artist ON song_list.artist_id = search_artist.artist_id WHERE NOT EXISTS (SELECT * FROM ban_artist WHERE ban_artist.artist_id = song_list.artist_id) AND NOT EXISTS (SELECT * FROM ban_track WHERE ban_track.track_id = song_list.track_id) AND NOT EXISTS (SELECT * FROM ban_ip WHERE ban_ip.ip = song_list.ip_address) GROUP BY track_id ORDER BY count DESC";
			foreach ($database->iterate($query) as $row) {
				$html .= "<tr><td>".utf8_encode($row->track_name_uncensored)."</td><td>".utf8_encode($row->artist_name_uncensored)."</td></tr>";
			}
			break;
	}
	$html .= "</table>";
	$pdf_controller->writeHTML($html, true, false, false, false, '');
	$pdf_controller->Output("song-suggestions.pdf", "D");
} else {
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<script type="text/javascript" src="//platform.ryantse.me/c2014/prom-songs-2012/js/jquery.min.js"></script>
		<script type="text/javascript" src="//platform.ryantse.me/c2014/prom-songs-2012/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="//platform.ryantse.me/c2014/prom-songs-2012/js/platform-admin.js"></script>
		
		<link rel="stylesheet" type="text/css" href="//platform.ryantse.me/c2014/prom-songs-2012/css/bootstrap.amelia.min.css" />
		<link rel="stylesheet" type="text/css" href="//platform.ryantse.me/c2014/prom-songs-2012/css/platform-admin.css" />
		<link rel="stylesheet" type="text/css" href="//platform.ryantse.me/c2014/prom-songs-2012/css/bootstrap-responsive.min.css" />
		
		<!--[if lt IE 9]>
			<script src="//platform.ryantse.me/c2014/prom-songs-2012/js/html5shiv.js"></script>
		<![endif]-->

		<title>IHS Prom '13 - Song Suggestions Administrator</title>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span3">
					<div class="well sidebar-nav">
						<ul class="nav nav-list" id="navigation">
							<li><a id="nav-home" href="?action=show&view=home">Home</a></li>
							<li class="nav-header">Manage Suggestions</li>
							<li><a id="nav-censored-suggestions" href="?action=show&view=censored-suggestions">Censored Suggestions</a></li>
							<li><a id="nav-uncensored-suggestions" href="?action=show&view=uncensored-suggestions">Uncensored Suggestions</a></li>
							<li><a id="nav-suggestions" href="?action=show&view=suggestions">All Suggestions</a></li>
							<li><a id="nav-artists" href="?action=show&view=artists">All Artists</a></li>
							<li class="nav-header">Manage Bans</li>
							<li><a id="nav-artist-bans" href="?action=show&view=artist-bans">Banned Artists</a></li>
							<li><a id="nav-song-bans" href="?action=show&view=song-bans">Banned Songs</a></li>
							<li><a id="nav-ip-bans" href="?action=show&view=ip-bans">Banned IPs</a></li>
							<li class="nav-header">Generate Reports</li>
							<li><a href="?action=generate&view=censored">Censored</a></li>
							<li><a href="?action=generate&view=uncensored">Uncensored</a></li>
						</ul>
					</div>
				</div>
				<div class="span9">
					<div id="home">
						<h2>IHS Prom '13 - Song Suggestions Administrator</h2>
						<div class="alert alert-info">
							Welcome to the song suggestions administrator panel. From here, you can manage student submissions for song suggestions.
						</div>
					</div>
					<div id="censored-suggestions">
						<h3>Suggested Songs <b>(Censored)</b></h3>
						<p>You are viewing all suggestions that have not been filtered by banned songs, artists, or IPs <b>ordered by most popularly requested</b>.</p>
						<table id="table" class="table table-hover table-bordered table-striped">
							<thead>
								<tr>
									<th>Song Name</th>
									<th>Artist</th>
									<th>Suggested</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div id="uncensored-suggestions">
						<h3>Suggested Songs <b>(Uncensored)</b></h3>
						<p>You are viewing all suggestions that have not been filtered by banned songs, artists, or IPs <b>ordered by most popularly requested</b>.</p>
						<table id="table" class="table table-hover table-bordered table-striped">
							<thead>
								<tr>
									<th>Song Name</th>
									<th>Artist</th>
									<th>Suggested</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div id="suggestions">
						<h3>All Suggestions</h3>
						<p>You are viewing all suggestions that have not been filtered by banned songs, artists, or IPs <b>ordered by most recent first</b>.</p>
						<table id="table" class="table table-hover table-bordered table-striped">
							<thead>
								<tr>
									<th>Song Name</th>
									<th>Artist</th>
									<th>Suggested By</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div id="artists">
						<h3>All Artists</h3>
						<p>You are viewing all suggestions that have not been filtered by banned artists.</p>
						<div class="alert alert-info">
							Note, the artist names are <u><b>not</b></u> automatically censored by default; if an artist name is inappropriate, please click on the artist name under the "Artist (Censored)" column to edit the censored name.
						</div>
						<table id="table" class="table table-hover table-bordered table-striped">
							<thead>
								<tr>
									<th>Artist (Uncensored)</th>
									<th>Artist (Censored)</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div id="banned-artists">
						<h3>Banned Artists</h3>
						<p>You are viewing all banned artists.</p>
						<table id="table" class="table table-hover table-bordered table-striped">
							<thead>
								<tr>
									<th>Artist</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div id="banned-songs">
						<h3>Banned Songs</h3>
						<p>You are viewing all banned songs.</p>
						<table id="table" class="table table-hover table-bordered table-striped">
							<thead>
								<tr>
									<th>Song</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div id="banned-ips">
						<h3>Banned IPs</h3>
						<p>You are viewing all banned IPs.</p>
						<table id="table" class="table table-hover table-bordered table-striped">
							<thead>
								<tr>
									<th>IP Address</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="footer">
				<p>&copy; IHS Class of 2014. Website written by Ryan Tse. All rights reserved.</p>
			</div>
		</div>
	</body>
</html>
<?php
}
?>