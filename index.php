<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		
		<meta property="og:title" content="Junior Prom 2013 - Song Suggestions" />
		<meta property="og:description" content="Suggest some of your favorite songs today and you might just hear them at prom!" />
		<meta property="og:image" content="https://platform.ryantse.me/c2014/prom-songs-2012/img/fb-share.png" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="http://irvington2014.tumblr.com/song-suggestions" />

		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<script type="text/javascript" src="//analytics.ryantse.me/piwik.js"></script>
		<script type="text/javascript" src="//platform.ryantse.me/c2014/prom-songs-2012/js/jquery.min.js"></script>
		<script type="text/javascript" src="//platform.ryantse.me/c2014/prom-songs-2012/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="//platform.ryantse.me/c2014/prom-songs-2012/js/placeholder.js"></script>
		<script type="text/javascript" src="//platform.ryantse.me/c2014/prom-songs-2012/js/platform.js"></script>
		
		<link rel="stylesheet" type="text/css" href="//platform.ryantse.me/c2014/prom-songs-2012/css/bootstrap.amelia.min.css" />
		<link rel="stylesheet" type="text/css" href="//platform.ryantse.me/c2014/prom-songs-2012/css/platform.css" />
		<link rel="stylesheet" type="text/css" href="//platform.ryantse.me/c2014/prom-songs-2012/css/bootstrap-responsive.min.css" />
		
		<!--[if lt IE 9]>
			<script src="//platform.ryantse.me/c2014/prom-songs-2012/js/html5shiv.js"></script>
		<![endif]-->

		<title>IHS Prom '13 - Song Suggestions</title>
	</head>
	<body>
		<div id="js">
			<div class="container-narrow">
				<div class="masthead">
					<ul class="nav nav-pills pull-right">
						<li><a id="nav-privacy" href="./">Privacy</a></li>
					</ul>
					<h3><a href="./">IHS Prom '13 - Song Suggestions</a></h3>
				</div>
				<form class="form-search" id="song-search-form">
					<div class="input-append">
						<input id="song-search-query" type="text" class="input-xxlarge search-query" placeholder="Search for a Song / Artist ...">
						<button id="song-search-button" type="search" class="btn btn-primary">Search</button>
					</div>
				</form>
				<table id="search-results" class="table table-hover table-bordered table-striped">
					<thead>
						<tr>
							<th>Song Name</th>
							<th>Artist</th>
							<th>Suggest</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="3"><b><i>To begin, find songs by name or artist using the search box above.</i></b></td>
						</tr>
					</tbody>
				</table>
				<div class="footer">
					<p>&copy; IHS Class of 2014. Website written by Ryan Tse. All rights reserved.</p>
				</div>
			</div>
		</div>
		<div id="nojs">
			<div class="container">
				<div id="privacy-header">
					<div class="alert alert-error"><b>Oops... Sorry!</b><br />Javascript is required for this website. Please enable Javascript or switch to a compatible browser to continue.</div>
					<h1>Privacy Policy</h1>
				</div>
				<div id="privacy-content">
					<h4>What information do we collect?</h4>
						<p>We collect information from you when you respond to a survey or fill out a form.</p>
					<h4>What do we use your information for?</h4>
						<p>Any of the information we collect from you may be used in one of the following ways:</p>
						<ul>
							<li>To personalize your experience<br />(your information helps us to better respond to your individual needs);</li>
							<li>To improve our website<br />(we continually strive to improve our website offerings based on the information and feedback we receive from you);</li>
							<li>To improve customer service<br />(your information helps us to more effectively respond to your customer service requests and support needs);</li>
							<li>To administer a contest, promotion, survey or other site feature.</li>
						</ul>
					<h4>How do we protect your information?</h4>
						<p>We implement a variety of security measures to maintain the safety of your personal information when you enter, submit, or access your personal information.</p>
						<p>We offer the use of a secure server. All supplied sensitive/credit information is transmitted via Secure Socket Layer (SSL) technology and then encrypted into our Database to be only accessed by those authorized with special access rights to our systems, and are required to keep the information confidential.</p>
						<p>After a transaction, your private information (credit cards, social security numbers, financials, etc.) will not be stored on our servers.</p>
					<h4>Do we use cookies?</h4>
						<p>Yes (Cookies are small files that a site or its service provider transfers to your computers hard drive through your Web browser (if you allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information).</p>
						<p>We use cookies to help us remember and process the items in your shopping cart, understand and save your preferences for future visits, keep track of advertisements and compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future. We may contract with third-party service providers to assist us in better understanding our site visitors. These service providers are not permitted to use the information collected on our behalf except to help us conduct and improve our business.</p>
					<h4>Do we disclose any information to outside parties?</h4>
						<p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>
					<h4>Third Party Links</h4>
						<p>Occasionally, at our discretion, we may include or offer third party products or services on our website. These third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>
					<h4>California Online Privacy Protection Act Compliance</h4>
						<p>Because we value your privacy we have taken the necessary precautions to be in compliance with the California Online Privacy Protection Act. We therefore will not distribute your personal information to outside parties without your consent.</p>
					<h4>Childrens Online Privacy Protection Act Compliance</h4>
						<p>We are in compliance with the requirements of COPPA (Childrens Online Privacy Protection Act); we do not collect any information from anyone under 13 years of age. Our website, products and services are all directed to people who are at least 13 years old or older.</p>
					<h4>Your Consent</h4>
						<p>By using our site, you consent to our web site privacy policy.</p>
					<h4>Changes to our Privacy Policy</h4>
						<p>If we decide to change our privacy policy, we will post those changes on this page, and/or update the Privacy Policy modification date below.</p>
						<p>This policy was last modified on March 22nd, 2013.</p>
					<h4>Contacting Us</h4>
						<p>If there are any questions regarding this privacy policy you may contact us using the information below.</p>
						<p>41800 Blacow Road<br />Fremont, California 94538<br />United States<br /></p>
				</div>
				<div class="footer">
					<p>&copy; IHS Class of 2014. Website written by Ryan Tse. All rights reserved.</p>
				</div>
			</div>
		</div>
	</body>
</html>