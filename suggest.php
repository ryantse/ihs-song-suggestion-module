<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: *");
header("Content-Type: application/json");

require("./mysql_catalyst.php");
require("./mysql_alchemy.php");
require("./mysql_blackmagic.php");
require("./config.php");

try {
	if(time() < strtotime($start_time)) {
		throw new Exception('{"status": "failure", "submissionerror_title": "Error Occurred", "submissionerror_message":"Sorry, the submission period has not begun. Please try again later."}');
	}
	if(time() > strtotime($end_time)) {
		throw new Exception('{"status": "failure", "submissionerror_title": "Error Occurred", "submissionerror_message":"Sorry, the submission period has already expired."}');
	}

	$artist_id                       = $_REQUEST["artistId"];
	$artist_name                     = $_REQUEST["artistName"];
	$track_id                        = $_REQUEST["trackId"];
	$track_name                      = $_REQUEST["trackName"];
	$track_censoredname              = $_REQUEST["trackCensoredName"];
	
	$submit_data_array               = array();
	$submit_data_array["time"]       = time();
	$submit_data_array["ip_address"] = $_SERVER["REMOTE_ADDR"];
	$submit_data_array["ip_port"]    = $_SERVER["REMOTE_PORT"];

	$submit_data = json_encode($submit_data_array);

	try {
		$database = MySQLDatabase::getInstance();
		$database->connect("localhost","ihsc2014_voting","FMRxFbJDLqWjwdvZ", "ihs_c2014songvoting");
	} catch (MySQLException $exception) {
		throw new Exception('{"status": "failure", "submissionerror_title": "Error Occurred", "submissionerror_message":"An unknown error occurred. Please try again later."}');
	}

	try {
		$query = "SELECT COUNT(*) FROM ban_artist WHERE artist_id='".mysql_real_escape_string($artist_id)."'";
		if($database->fetchOne($query) > 0) {
			throw new Exception('{"status": "failure", "submissionerror_title": "Artist Banned", "submissionerror_message":"This artist has been banned by an administrator, please select another song."}');
		}
	} catch (MySQLException $exception) {
		throw new Exception('{"status": "failure", "submissionerror_title": "Error Occurred", "submissionerror_message":"An unknown error occurred. Please try again later."}');
	}

	try {
		$query = "SELECT COUNT(*) FROM ban_track WHERE track_id='".mysql_real_escape_string($artist_id)."'";
		if($database->fetchOne($query) > 0) {
			throw new Exception('{"status": "failure", "submissionerror_title": "Song Banned", "submissionerror_message":"This song has been banned by an administrator, please select another song."}');
		}
	} catch (MySQLException $exception) {
		throw new Exception('{"status": "failure", "submissionerror_title": "Error Occurred", "submissionerror_message":"An unknown error occurred. Please try again later."}');
	}

	try {
		$query = "SELECT COUNT(*) FROM ban_ip WHERE ip='".mysql_real_escape_string($_SERVER["REMOTE_ADDR"])."'";
		if($database->fetchOne($query) > 0) {
			throw new Exception('{"status":"success", "target": "'.$artist_id.'_'.$track_id.'"}');
		}
	} catch (MySQLException $exception) {
		throw new Exception('{"status": "failure", "submissionerror_title": "Error Occurred", "submissionerror_message":"An unknown error occurred. Please try again later."}');
	}

	try {
		$query = "INSERT IGNORE INTO artist_list (artist_id, artist_uncensored, artist_censored) VALUES ('".mysql_real_escape_string($artist_id)."','".mysql_real_escape_string($artist_name)."','".mysql_real_escape_string($artist_name)."')";
		$database->Query($query);
	} catch (MySQLException $exception) {
		throw new Exception('{"status": "failure", "submissionerror_title": "Error Occurred", "submissionerror_message":"An unknown error occurred. Please try again later."}');
	}

	try {
		$query = "INSERT IGNORE INTO track_list (track_id, track_uncensored, track_censored) VALUES ('".mysql_real_escape_string($track_id)."','".mysql_real_escape_string($track_name)."','".mysql_real_escape_string($track_censoredname)."')";
		$database->Query($query);
	} catch (MySQLException $exception) {
		throw new Exception('{"status": "failure", "submissionerror_title": "Error Occurred", "submissionerror_message":"An unknown error occurred. Please try again later."}');
	}

	try {
		$query = "SELECT COUNT(*) FROM song_list WHERE track_id='".mysql_real_escape_string($track_id)."' AND artist_id='".mysql_real_escape_string($artist_id)."' AND ip_address='".mysql_real_escape_string($_SERVER["REMOTE_ADDR"])."'";
		if($database->fetchOne($query) < 1) {
			$query = "INSERT INTO song_list (track_id, artist_id, ip_address, submit_data) VALUES ('".mysql_real_escape_string($track_id)."','".mysql_real_escape_string($artist_id)."','".mysql_real_escape_string($_SERVER["REMOTE_ADDR"])."','".mysql_real_escape_string($submit_data)."')";
			$database->Query($query);
		}
	} catch (MySQLException $exception) {
		throw new Exception('{"status": "failure", "submissionerror_title": "Error Occurred", "submissionerror_message":"An unknown error occurred. Please try again later."}');
	}

	$return_status = '{"status":"success", "target": "'.$artist_id.'_'.$track_id.'"}';

} catch (Exception $exception) {
	$return_status = $exception->getMessage();
}

?>
<?php echo $_REQUEST["callback"]; ?>(<?php echo $return_status; ?>);