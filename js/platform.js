jQuery.noConflict();
jQuery.ajaxSettings.cache = false;
jQuery.ajaxSetup({"async": false});
var last_query = "";

jQuery(document).ready(function() {
	initialize_privacyview();
	initialize_searchform();
	initialize_tracking();
});

function initialize_searchform() {
	jQuery("#js").attr("id", "js-enabled");
	jQuery("#song-search-form").submit(song_search);
	jQuery("#song-search-query").placeholder();
	jQuery("#song-search-query").focus();
}

function initialize_privacyview() {
	jQuery("#nojs").hide();
	jQuery("#nav-privacy").click(function() {
		var privacy_policy_text = jQuery("#privacy-content").html();
		jQuery('<div class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="privacy-title" aria-hidden="false" style="display: block;"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h3 id="privacy-title">Privacy Policy</h3></div><div class="modal-body">'+privacy_policy_text+'</div><div class="modal-footer"><a class="btn btn-primary" data-dismiss="modal">Close</a></div></div>').modal({backdrop: "static"});
		return false;
	});
}

function initialize_tracking() {
	var pkBaseURL = "https://analytics.ryantse.me/";
	try {
		var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 6);
		piwikTracker.trackPageView();
		piwikTracker.enableLinkTracking();
	} catch( err ) {}
}

function song_search() {
	if(jQuery("#song-search-query").val() !== "" && jQuery("#song-search-query").val() !== last_query) {
		jQuery("#search-results tbody").empty();
		jQuery("#search-results tbody").append('<tr><td colspan="3"><b><i>Loading ...</i></b></td></tr>')
		jQuery("#song-search-query").blur();
		last_query = jQuery("#song-search-query").val();
		jQuery.getJSON("https://itunes.apple.com/search?callback=?", {
			"country": "us",
			"explicit": "y",
			"media": "music",
			"limit": 25,
			"term": song_filter(jQuery("#song-search-query").val())
		}, song_results);
	}
	return false;
}

function song_filter(input) {
	return input;	
}

function song_results(data) {
	jQuery("#search-results tbody").empty();
	if(data.resultCount < 1) {
		jQuery("#search-results tbody").html("<tr><td colspan='3'><b>No Results Found</b></td></tr>");
	} else {
		for(var i=0; i < data.resultCount; i++) {
			var search_result = jQuery("<tr id='"+data.results[i].artistId+"_"+data.results[i].trackId+"'><td>"+data.results[i].trackName+"</td><td>"+data.results[i].artistName+"</td><td><a class='btn'>Select</a></td></tr>");
			search_result.data("artistId", data.results[i].artistId);
			search_result.data("artistName",data.results[i].artistName);
			search_result.data("trackId", data.results[i].trackId);
			search_result.data("trackName", data.results[i].trackName);
			search_result.data("trackCensoredName", data.results[i].trackCensoredName);
			search_result.find("a").click(function(event) {
				jQuery.getJSON("https://platform.ryantse.me/c2014/prom-songs-2012/suggest.php?callback=?", {
					artistId: jQuery(event.target).parents("tr").data("artistId"),
					artistName: jQuery(event.target).parents("tr").data("artistName"),
					trackId: jQuery(event.target).parents("tr").data("trackId"),
					trackName: jQuery(event.target).parents("tr").data("trackName"),
					trackCensoredName: jQuery(event.target).parents("tr").data("trackCensoredName")
				}, song_suggestion_result);
			});
			jQuery("#search-results tbody").append(search_result);
		}
	}
}

function song_suggestion_result(data) {
	switch(data.status) {
		case "failure":
			jQuery('<div class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="error-title" aria-hidden="false" style="display: block;"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h3 id="error-title">'+data.submissionerror_title+'</h3></div><div class="modal-body">'+data.submissionerror_message+'</div><div class="modal-footer"><a class="btn btn-primary" data-dismiss="modal">Dismiss</a></div></div>').modal({backdrop: "static", keyboard: true});
			break;

		case "success":
			jQuery("#"+data.target).find("a").unbind().addClass("btn-success").text("Added!");
			break;
	}
}