jQuery.noConflict();
jQuery.ajaxSettings.cache = false;
jQuery.ajaxSetup({"async": false});

jQuery(document).ready(function() {
	initialize_links();
	jQuery("#nav-home").click();
});

function initialize_links() {
	jQuery("#nav-home").click(show_home);
	
	jQuery("#nav-censored-suggestions").click(show_censored_suggestions);
	jQuery("#nav-uncensored-suggestions").click(show_uncensored_suggestions);
	jQuery("#nav-suggestions").click(show_suggestions);
	jQuery("#nav-artists").click(show_artists);

	jQuery("#nav-artist-bans").click(show_artist_bans);
	jQuery("#nav-song-bans").click(show_song_bans);
	jQuery("#nav-ip-bans").click(show_ip_bans);
}

function hide_all() {
	jQuery("#navigation").find("li").removeClass("active");

	jQuery("#home").hide();
	
	jQuery("#censored-suggestions").hide();
	jQuery("#uncensored-suggestions").hide();
	jQuery("#suggestions").hide();
	jQuery("#artists").hide();

	jQuery("#banned-artists").hide();
	jQuery("#banned-songs").hide();
	jQuery("#banned-ips").hide();
}

function show_home(event) {
	hide_all();
	jQuery(event.target).parents("li").addClass("active");
	jQuery("#home").show();
	jQuery.getJSON("?action=query&view=statistics", {}, show_home_statistics);
	return false;
}

function show_home_statistics(data) {
	try {
		jQuery("#home").find("#statistics").remove();
	} catch (e) {}
	jQuery("#home").append("<div class='alert alert-warning' id='statistics'><h4>Statistics</h4>Accepted Suggestions: "+(data.acceptedSubmissions)+" ("+((data.acceptedSubmissions/data.totalSubmissions*100).toFixed(2))+"%)<br />Total Suggestions: "+(data.totalSubmissions)+"<br /></div>");
}

function show_censored_suggestions(event) {
	hide_all();
	jQuery(event.target).parents("li").addClass("active");
	jQuery("#censored-suggestions").show();
	jQuery.getJSON("?action=query&view=censored-suggestions", {}, show_censored_suggestions_list);
	return false;
}

function show_censored_suggestions_list(data) {
	table_content = jQuery("#censored-suggestions").find("#table").find("tbody");
	table_content.empty();
	if(data.resultCount < 1) {
		table_content.html("<tr><td colspan='4'><b>No Suggestions Found</b></td></tr>");
	} else {
		for(var i=0; i < data.resultCount; i++) {
			var list_result = jQuery("<tr id='"+data.results[i].song_id+"_"+data.results[i].artist_id+"'><td>"+data.results[i].song_name+"</td><td>"+data.results[i].artist_name+"</td><td>"+data.results[i].count+" Time(s)</td><td><div class='btn-group'><button class='btn dropdown-toggle' data-toggle='dropdown'>Manage <span class='caret'></span></button><ul class='dropdown-menu'><li><a id='ban-song' href='?nojs'>Ban Song</a></li><li><a id='ban-artist' href='?nojs'>Ban Artist</a></li></ul></td></tr>");
			list_result.data("songId", data.results[i].song_id);
			list_result.data("artistId", data.results[i].artist_id);
			list_result.find("#ban-artist").click(function(event) {
				jQuery.getJSON("?", {
					action: "query",
					view: "remove",
					type: "artist",
					target: jQuery(event.target).parents("tr").data("artistId")
				});
				suggestion_list_remove_artist(jQuery(event.target).parents("tr").data("artistId"));
				jQuery('[data-toggle="dropdown"]').parent().removeClass('open');
				return false;
			});
			list_result.find("#ban-song").click(function(event) {
				jQuery.getJSON("?", {
					action: "query",
					view: "remove",
					type: "song",
					target: jQuery(event.target).parents("tr").data("songId")
				});
				suggestion_list_remove_song(jQuery(event.target).parents("tr").data("songId"));
				jQuery('[data-toggle="dropdown"]').parent().removeClass('open');
				return false;
			});
			table_content.append(list_result);
		}
	}
}

function show_uncensored_suggestions(event) {
	hide_all();
	jQuery(event.target).parents("li").addClass("active");
	jQuery("#uncensored-suggestions").show();
	jQuery.getJSON("?action=query&view=uncensored-suggestions", {}, show_uncensored_suggestions_list);
	return false;
}

function show_uncensored_suggestions_list(data) {
	table_content = jQuery("#uncensored-suggestions").find("#table").find("tbody");
	table_content.empty();
	if(data.resultCount < 1) {
		table_content.html("<tr><td colspan='4'><b>No Suggestions Found</b></td></tr>");
	} else {
		for(var i=0; i < data.resultCount; i++) {
			var list_result = jQuery("<tr id='"+data.results[i].song_id+"_"+data.results[i].artist_id+"'><td>"+data.results[i].song_name+"</td><td>"+data.results[i].artist_name+"</td><td>"+data.results[i].count+" Time(s)</td><td><div class='btn-group'><button class='btn dropdown-toggle' data-toggle='dropdown'>Manage <span class='caret'></span></button><ul class='dropdown-menu'><li><a id='ban-song' href='?nojs'>Ban Song</a></li><li><a id='ban-artist' href='?nojs'>Ban Artist</a></li></ul></td></tr>");
			list_result.data("songId", data.results[i].song_id);
			list_result.data("artistId", data.results[i].artist_id);
			list_result.find("#ban-artist").click(function(event) {
				jQuery.getJSON("?", {
					action: "query",
					view: "remove",
					type: "artist",
					target: jQuery(event.target).parents("tr").data("artistId")
				});
				suggestion_list_remove_artist(jQuery(event.target).parents("tr").data("artistId"));
				jQuery('[data-toggle="dropdown"]').parent().removeClass('open');
				return false;
			});
			list_result.find("#ban-song").click(function(event) {
				jQuery.getJSON("?", {
					action: "query",
					view: "remove",
					type: "song",
					target: jQuery(event.target).parents("tr").data("songId")
				});
				suggestion_list_remove_song(jQuery(event.target).parents("tr").data("songId"));
				jQuery('[data-toggle="dropdown"]').parent().removeClass('open');
				return false;
			});
			table_content.append(list_result);
		}
	}
}

function show_suggestions(event) {
	hide_all();
	jQuery(event.target).parents("li").addClass("active");
	jQuery("#suggestions").show();
	jQuery.getJSON("?action=query&view=suggestions", {}, show_suggestions_list);
	return false;
}

function show_suggestions_list(data) {
	table_content = jQuery("#suggestions").find("#table").find("tbody");
	table_content.empty();
	if(data.resultCount < 1) {
		table_content.html("<tr><td colspan='4'><b>No Suggestions Found</b></td></tr>");
	} else {
		for(var i=0; i < data.resultCount; i++) {
			var list_result = jQuery("<tr id='"+data.results[i].song_id+"_"+data.results[i].ip_address+"_"+data.results[i].artist_id+"'><td>"+data.results[i].song_name+"</td><td>"+data.results[i].artist_name+"</td><td>"+data.results[i].ip_address+"</td><td><div class='btn-group'><button class='btn dropdown-toggle' data-toggle='dropdown'>Manage <span class='caret'></span></button><ul class='dropdown-menu'><li><a id='ban-song' href='?nojs'>Ban Song</a></li><li><a id='ban-artist' href='?nojs'>Ban Artist</a></li><li><a id='ban-ip' href='?nojs'>Ban IP</a></li></ul></td></tr>");
			list_result.data("songId", data.results[i].song_id);
			list_result.data("artistId", data.results[i].artist_id);
			list_result.data("ip_address", data.results[i].ip_address);
			list_result.find("#ban-artist").click(function(event) {
				jQuery.getJSON("?", {
					action: "query",
					view: "remove",
					type: "artist",
					target: jQuery(event.target).parents("tr").data("artistId")
				});
				suggestion_list_remove_artist(jQuery(event.target).parents("tr").data("artistId"));
				jQuery('[data-toggle="dropdown"]').parent().removeClass('open');
				return false;
			});
			list_result.find("#ban-song").click(function(event) {
				jQuery.getJSON("?", {
					action: "query",
					view: "remove",
					type: "song",
					target: jQuery(event.target).parents("tr").data("songId")
				});
				suggestion_list_remove_song(jQuery(event.target).parents("tr").data("songId"));
				jQuery('[data-toggle="dropdown"]').parent().removeClass('open');
				return false;
			});
			list_result.find("#ban-ip").click(function(event) {
				jQuery.getJSON("?", {
					action: "query",
					view: "remove",
					type: "ip",
					target: jQuery(event.target).parents("tr").data("ip_address")
				});
				suggestion_list_remove_ip(jQuery(event.target).parents("tr").data("ip_address"));
				jQuery('[data-toggle="dropdown"]').parent().removeClass('open');
				return false;
			});
			table_content.append(list_result);
		}
	}
}

function suggestion_list_remove_song(song) {
	jQuery("tr[id^='"+song+"_']").slideUp("normal", function() { jQuery(this).remove(); });
}

function suggestion_list_remove_artist(artist) {
	jQuery("tr[id$='_"+artist+"']").slideUp("normal", function() { jQuery(this).remove(); });
}

function suggestion_list_remove_ip(ip) {
	jQuery("tr[id*='_"+ip+"_']").slideUp("normal", function() { jQuery(this).remove(); });
}

function show_artists(event) {
	hide_all();
	jQuery(event.target).parents("li").addClass("active");
	jQuery("#artists").show();
	jQuery.getJSON("?action=query&view=artists", {}, show_artists_list);
	return false;
}

function show_artists_list(data) {
	table_content = jQuery("#artists").find("#table").find("tbody");
	table_content.empty();
	if(data.resultCount < 1) {
		table_content.html("<tr><td colspan='3'><b>No Artists Found</b></td></tr>");
	} else {
		for(var i=0; i < data.resultCount; i++) {
			var list_result = jQuery("<tr id='"+data.results[i].artist_id+"'><td>"+data.results[i].uncensored+"</td><td id='censored-edit'>"+data.results[i].censored+"</td><td><div class='btn-group'><button class='btn dropdown-toggle' data-toggle='dropdown'>Manage <span class='caret'></span></button><ul class='dropdown-menu'><li><a id='ban-artist' href='?nojs'>Ban Artist</a></li></ul></td></tr>");
			list_result.data("artistId", data.results[i].artist_id);
			list_result.find("#ban-artist").click(function(event) {
				jQuery.getJSON("?", {
					action: "query",
					view: "remove",
					type: "artist",
					target: jQuery(event.target).parents("tr").data("artistId")
				});
				artists_list_remove_artist(jQuery(event.target).parents("tr").data("artistId"));
				jQuery('[data-toggle="dropdown"]').parent().removeClass('open');
				return false;
			});
			list_result.find("#censored-edit").click(censored_edit);
			table_content.append(list_result);
		}
	}
}

function censored_edit(event) {
	var target = jQuery(event.target);
	target.unbind();
	target.attr("id", "");
	var original_value = target.text();
	var censored_editor = jQuery("<input type='text'>");
	censored_editor.attr("value", original_value);
	censored_editor.keydown(function(event) {
		if(event.keyCode == 9 || event.keyCode == 13) {
			jQuery.getJSON("?", {
				action: "query",
				view: "modify",
				type: "artist",
				target: jQuery(event.target).parents("tr").data("artistId"),
				value: censored_editor.val()
			});
			target.text(censored_editor.val());
			target.click(censored_edit);
			target.attr("id", "censored-edit");
		} else if (event.keyCode == 27) {
			target.text(original_value);
			target.click(censored_edit);
			target.attr("id", "censored-edit");
		}
	});
	censored_editor.blur(function(event) {
		target.text(original_value);
		target.click(censored_edit);
		target.attr("id", "censored-edit");
	});
	target.html(censored_editor);
	censored_editor.focus();
}

function artists_list_remove_artist(artist) {
	jQuery("tr[id^='"+artist+"']").slideUp("normal", function() { jQuery(this).remove(); });
}

function show_artist_bans(event) {
	hide_all();
	jQuery(event.target).parents("li").addClass("active");
	jQuery("#banned-artists").show();
	jQuery.getJSON("?action=query&view=banned-artists", {}, show_banned_artist_list);
	return false;
}

function show_banned_artist_list(data) {
	table_content = jQuery("#banned-artists").find("#table").find("tbody");
	table_content.empty();
	if(data.resultCount < 1) {
		table_content.html("<tr><td colspan='2'><b>No Banned Artists</b></td></tr>");
	} else {
		for(var i=0; i < data.resultCount; i++) {
			var list_result = jQuery("<tr id='"+data.results[i].artist_id+"'><td>"+data.results[i].artist_name+"</td><td><div id='unban-artist' class='btn'>Unban Artist</div></td></tr>");
			list_result.data("artistId", data.results[i].artist_id);
			list_result.find("#unban-artist").click(function(event) {
				jQuery.getJSON("?", {
					action: "query",
					view: "unban",
					type: "artist",
					target: jQuery(event.target).parents("tr").data("artistId")
				});
				banned_artist_list_remove_artist(jQuery(event.target).parents("tr").data("artistId"));
				jQuery('[data-toggle="dropdown"]').parent().removeClass('open');
				return false;
			});
			table_content.append(list_result);
		}
	}
}

function banned_artist_list_remove_artist(artist) {
	jQuery("tr[id^='"+artist+"']").slideUp("normal", function() { jQuery(this).remove(); });
}

function show_song_bans(event) {
	hide_all();
	jQuery(event.target).parents("li").addClass("active");
	jQuery("#banned-songs").show();
	jQuery.getJSON("?action=query&view=banned-songs", {}, show_banned_song_list);
	return false;
}

function show_banned_song_list(data) {
	table_content = jQuery("#banned-songs").find("#table").find("tbody");
	table_content.empty();
	if(data.resultCount < 1) {
		table_content.html("<tr><td colspan='2'><b>No Banned Songs</b></td></tr>");
	} else {
		for(var i=0; i < data.resultCount; i++) {
			var list_result = jQuery("<tr id='"+data.results[i].song_id+"'><td>"+data.results[i].song_name+"</td><td><div id='unban-song' class='btn'>Unban Song</div></td></tr>");
			list_result.data("songId", data.results[i].song_id);
			list_result.find("#unban-song").click(function(event) {
				jQuery.getJSON("?", {
					action: "query",
					view: "unban",
					type: "song",
					target: jQuery(event.target).parents("tr").data("songId")
				});
				banned_song_list_remove_song(jQuery(event.target).parents("tr").data("songId"));
				jQuery('[data-toggle="dropdown"]').parent().removeClass('open');
				return false;
			});
			table_content.append(list_result);
		}
	}
}

function banned_song_list_remove_song(song) {
	jQuery("tr[id^='"+song+"']").slideUp("normal", function() { jQuery(this).remove(); });
}

function show_ip_bans(event) {
	hide_all();
	jQuery(event.target).parents("li").addClass("active");
	jQuery("#banned-ips").show();
	jQuery.getJSON("?action=query&view=banned-ips", {}, show_banned_ip_list);
	return false;
}

function show_banned_ip_list(data) {
	table_content = jQuery("#banned-ips").find("#table").find("tbody");
	table_content.empty();
	if(data.resultCount < 1) {
		table_content.html("<tr><td colspan='2'><b>No Banned IPs</b></td></tr>");
	} else {
		for(var i=0; i < data.resultCount; i++) {
			var list_result = jQuery("<tr id='"+data.results[i].ip+"'><td>"+data.results[i].ip+"</td><td><div id='unban-ip' class='btn'>Unban IP</div></td></tr>");
			list_result.data("ip", data.results[i].ip);
			list_result.find("#unban-ip").click(function(event) {
				jQuery.getJSON("?", {
					action: "query",
					view: "unban",
					type: "ip",
					target: jQuery(event.target).parents("tr").data("ip")
				});
				banned_ip_list_remove_ip(jQuery(event.target).parents("tr").data("ip"));
				jQuery('[data-toggle="dropdown"]').parent().removeClass('open');
				return false;
			});
			table_content.append(list_result);
		}
	}
}

function banned_ip_list_remove_ip(ip) {
	jQuery("tr[id^='"+ip+"']").slideUp("normal", function() { jQuery(this).remove(); });
}